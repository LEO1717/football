package ru.yarosh.football;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Button;
import org.w3c.dom.Text;

public class MyActivity extends Activity {

    private Integer int_first_com = 0;
    private Integer int_second_com = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }

    /**
     * Метод изменяет фон и счет первой команды
     *
     * @param view
     */
    public void onClickBtnAddAccountFirst(View view){
        String str_first_command = "";
        String str_second_command = "";

        TextView txt_first_command = (TextView) findViewById(R.id.txt_first_command);
        str_first_command = txt_first_command.getText().toString();
        int_first_com = Integer.parseInt(str_first_command);
        int_first_com++;
        txt_first_command.setText(int_first_com.toString());

        TextView txt_second_command = (TextView) findViewById(R.id.txt_second_command);
        str_second_command = txt_second_command.getText().toString();
        int_second_com = Integer.parseInt(str_second_command);

        LinearLayout mainLayout = (LinearLayout) findViewById(R.id.mainLayout);
        if(int_first_com == int_second_com) {
            mainLayout.setBackgroundColor(Color.parseColor("#8B8B8B"));
        }
        else mainLayout.setBackgroundColor(Color.parseColor("#D30700"));
    }

    /**
     * Метод изменяет фон и счет второй команды
     *
     * @param view
     */
    public void onClickBtnAddAccountSecond(View view){
        String str_first_command = "";
        String str_second_command = "";

        TextView txt_second_command = (TextView) findViewById(R.id.txt_second_command);
        str_second_command = txt_second_command.getText().toString();
        int_second_com = Integer.parseInt(str_second_command);
        int_second_com++;
        txt_second_command.setText(int_second_com.toString());

        TextView txt_first_command = (TextView) findViewById(R.id.txt_first_command);
        str_first_command = txt_first_command.getText().toString();
        int_first_com = Integer.parseInt(str_first_command);

        LinearLayout mainLayout = (LinearLayout) findViewById(R.id.mainLayout);
        if(int_first_com == int_second_com) {
            mainLayout.setBackgroundColor(Color.parseColor("#8B8B8B"));
        }
        else mainLayout.setBackgroundColor(Color.parseColor("#1800A0"));
    }

    public void onClickBtnClearAccount(View view){
        Integer clear = 0;

        TextView txt_first_command = (TextView) findViewById(R.id.txt_first_command);
        TextView txt_second_command = (TextView) findViewById(R.id.txt_second_command);
        LinearLayout mainLayout = (LinearLayout) findViewById(R.id.mainLayout);

        txt_first_command.setText(clear.toString());
        txt_second_command.setText(clear.toString());

        mainLayout.setBackgroundColor(Color.BLACK);

    }
}
